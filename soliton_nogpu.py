#####################################################################################################
########################## 1D Dark or Bright Soliton Propagation ####################################
###################################### Non-GPU version ##############################################
################################## By: J. R. C. C. C. Correia #######################################
#####################################################################################################

import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt
from scipy.integrate import simps
import timeit

#####################################################################################################
#The next two functions contain the analytical solutions of the bright (bright_func)
# and the dark (dark_func) soliton solutions of 1D Gross-Pitaevskii equation
def bright_func(x,x0=0.0,nu=0.5,mu=0.05,delta0=0.0,t=0.0):
        return 2*nu*Sech(nu,x,x0,mu,t)*np.exp( 1.0j*mu*(x-x0) + 1.0j*delta_func(t,nu,mu,delta0) )

def dark_func(x,x0=0.0,theta=0.0,psi_0=1.0,t=0.0):
	A=np.sin(theta)
	B=np.cos(theta)
	return psi_0*(B*np.tanh(psi_0*B* (x - x0 - A*psi_0*t) ) + 1.0j*A) * np.exp(-1.0j*t*psi_0**2)

#####################################################################################################
#delta_func describes the evolution of the phase for the bright soliton solution
def delta_func(t,nu,mu,delta0):
	return (2*(nu**2) - 0.5 * (mu**2)) * t + delta0

def Sech(nu,x,x0,mu,t):
	return 1/np.cosh(2*nu*(x-x0-mu*t))

#####################################################################################################
#k_func returns the k-values used for the Fourier Transform of the Laplacian
def k_func(N,dx,x):
        kx = fft.fftfreq(N,d=dx) * 2.0 * np.pi
        return kx

#####################################################################################################
#This function returns the envelop used in the dark soliton, as a way to mitigate
# the nefarious effects of boundary conditions
def envelop(L,x,k=1.0):
	a= -L
	b=-a
	return Heavi(x,k,a)-Heavi(x,k,b)

#####################################################################################################
#A continuous approximation to the Heaviside function (used by the previous envelope function)
def Heavi(x,k,const):
	return 1.0 / (1.0 + np.exp(-2.0* k * (x-const) ))

size_box=512
L=40.0
x=np.float64(np.linspace(-L,L,size_box))
dx=abs(x[1]-x[0])
initpos=0.0
g=np.float64(-1.0)

#Set the initial conditions to be equal to the analytical solution at t=0
if g > 0.0:
	psi=np.complex128(bright_func(x,x0=initpos))
	folder = 'Bright_nogpu'
elif g < 0.0:
	psi = np.complex128(dark_func(x,x0=initpos,theta=0.0,psi_0=1.0)*envelop(L,x))
	folder = 'Dark_nogpu'

dt = np.float64(0.01)
tfinal = np.float64(1.0e2)
time = np.float64(np.arange(dt,tfinal,dt))
kx = np.float64(k_func(size_box,dx,x))
jcom=np.complex128(1.0j)

lin= np.exp(-0.5 * jcom * 0.5 * dt * kx**2)

vmin=min(abs(psi)**2)
vmax=1.6*max(abs(psi)**2)
erstream=open(folder+'/error.txt','w')

start_time=timeit.default_timer()

#The Lines mentioned in the below cycle are the steps described on the report on page 3.
for t in range(len(time)):

	#Line 1
	#Compute psi in Fourier space
	psi_m = fft.fft(psi)

	#Line 2
	#Evolve for a half step in Fourier space
	psi_m = psi_m * lin
	#return to direct space
	psi = fft.ifft(psi_m)

	#Line 3
	#evolve for a full non-linear step
	psi = psi * np.exp(jcom * g * dt * np.conj(psi) *psi)

 	#Line 4
	#Go back to Fourier space
	psi_m = fft.fft(psi)

	#Line 5
	#Evolve for a half-step in Fourier space
	psi_m = psi_m * lin
	#Finish with Inverse transform (return to discrete space)
	psi = fft.ifft(psi_m)

	if (t % 1000) == 0:
		#Plot the results, compare with the analytical expression in order to find
		#the relative error.
		plt.clf()
		plt.plot(x,abs(psi)**2,'b')
		if g > 0.0: psi_analytic=np.complex128(bright_func(x,x0=initpos,t=time[t]))
		if g < 0.0: psi_analytic=np.complex128(dark_func(x,x0=initpos,theta=0.0,psi_0=1.0,t=time[t])*envelop(L,x))
		plt.hold(True)
		plt.plot(x,abs(psi_analytic)**2,'r')
		plt.ylim(vmin,vmax)
		fname='/iter%05d.png'%(t+1)
		plt.savefig(folder+fname)

		error = simps(abs(psi-psi_analytic)**2) / simps(abs(psi)**2)
		erstream.write(str(error)+'\n')

ttaken=timeit.default_timer() - start_time
print ttaken
erstream.close()
