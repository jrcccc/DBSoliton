################INSTALLATION################

This is a Git repository with the necessary script for this project. To install simply do,
git clone
https://gitlab.com/jrcccc/DBSoliton.git
To run the non-GPU version do,
python soliton_nogpu.py
For the GPU version first edit soliton.py, line 23 to either,


• api=cluda.ocl_api()


• api=cluda.cuda_api()


Depending on wether you wish to use OpenCL or CUDA.
To run do,
python soliton .py
in the folder where the code is located.


#############SYSTEM REQUIREMENTS##############

The are some software requirements shared by both the GPU (Graphical Processing Unit) and non-GPU version of the code. Both require:


• Python 2.6 (at least 2.6.1, though the code was developed using 2.7.11);


• Numpy 1.2.1 (1.10.1 was used);


• Matplotlib 1.5.0 (other versions may be suitable, in principle);


For the GPU version on should have a (GPU) compatible with Nvidia’s Compute Unified Device Archi- tecture (CUDA) or with the Open Computing Lan- guage (OpenCL).


• Reikna 0.6.6 (though later on we used 0.7.0dev);


• CUDA Toolkit and the PyCUDA package or,


• An OpenCL implementation and the PyOpenCL package.


#############FURTHER IMPROVEMENTS##############

DISCLAIMER: I am having issues with obtaining the norm of Phi using reikna, thus, for now it uses the CPU to do so (obvious performance hit). 
This issue will be fixed in a near future.
