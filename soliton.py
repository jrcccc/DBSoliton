#####################################################################################################
########################## 1D Dark or Bright Soliton Propagation ####################################
######################################## GPU version ################################################
################################## By: J. R. C. C. C. Correia #######################################
#####################################################################################################

import numpy as np
from scipy.integrate import simps
import matplotlib.pyplot as plt
import timeit

import reikna.cluda as cluda
from reikna.fft import FFT
import reikna.cluda.functions as functions

#####################################################################################################
#The next two functions contain the analytical solutions of the bright (bright_func)
# and the dark (dark_func) soliton solutions of 1D Gross-Pitaevskii equation
def bright_func(x,x0=0.0,nu=0.5,mu=0.05,delta0=0.0,t=0.0):
	return ( 2*nu*np.exp( 1.0j*mu*(x-x0) + 1.0j*delta_func(t,nu,mu,delta0) ) ) / np.cosh(2*nu*(x -x0 -mu*t))

#####################################################################################################
def dark_func(x,x0=0.0,theta=0.0,psi_0=1.0,t=0.0):
	A=np.sin(theta)
	B=np.cos(theta)
	return psi_0*(B*np.tanh(psi_0*B* (x - x0 - A*psi_0*t) ) + 1.0j*A) * np.exp(-1.0j*t*psi_0**2)

#####################################################################################################
#delta_func describes the evolution of the phase for the bright soliton solution
def delta_func(t,nu,mu,delta0):
	return (2*nu**2 - 0.5 *mu**2) * t + delta0

#####################################################################################################
#k_func returns the k-values used for the Fourier Transform of the Laplacian
def k_func(N,dx,x):
        kx = np.fft.fftfreq(N,d=dx) * 2.0 * np.pi
        return kx

#####################################################################################################
#This function returns the envelop used in the dark soliton, as a way to mitigate
# the nefarious effects of boundary conditions
def envelop(L,x,k=1.0):
        a= -L
        b=-a
        return Heavi(x,k,a)-Heavi(x,k,b)

#####################################################################################################
#A continuous approximation to the Heaviside function (used by the previous envelope function)
def Heavi(x,k,const):
        return 1.0 / (1.0 + np.exp(-2.0* k * (x-const) ))

#####################################################################################################
#Specify which API we are using:
# - for CUDA use cluda.cuda_api()
# - for OpenCL use cluda.ocl_api()
api=cluda.ocl_api()
#Create a thread where we  shall compile all pieces of GPU code
# In CUDA this is equivalent to a stream, in OpenCL to a context
thr=api.Thread.create()

size_box=512
L=40.0
x=np.linspace(-L,L,size_box)
initpos=0.0
g=np.float32(1.0)

#Set the initial conditions to be equal to the analytical solution at t=0
if g > 0.0:
	psi=np.complex64(bright_func(x,x0=initpos))
	folder = 'Bright'
elif g < 0.0:
	psi=np.complex64(dark_func(x,x0=initpos,theta=0.0,psi_0=1.0)*envelop(L,x))
	folder = 'Dark'

dt = np.float32(0.01)
tfinal = np.float32(1.0e2)

time = np.arange(dt,tfinal,dt)
dx=abs(x[0]-x[1])
kx = np.float32(k_func(size_box,dx,x))
jcom=np.complex64(1.0j)

lin= np.exp(-0.5 * jcom * 0.5 * dt * kx**2)

#Once we have several important variables declared,
# we can push them to the GPU's VRAM using thr.to_device.
# Another reasonable way to reserve space in memory
#for our variables is to create an empty array (which
# we later populate) using empty_like(some_variable_onGPU).
# These are all stored in Global Memory.
glin = thr.to_device(lin)
gnonlin = thr.empty_like(glin)

vmin = min(abs(psi)**2)
vmax = 1.6*max(abs(psi)**2)

gpsi = thr.to_device(psi)
gpsim = thr.empty_like(gpsi)

#FM - Boolean containing wether to use Fast Math
# in the compilation of the Kernels or not.
# I advise keeping at False ;)
FM = False

#Here we compile the fft computation for a given thread, with Fast Math set to FM
# and we tell it to expect an input with similar attributes to the array gpsi
fftc = (FFT(gpsi)).compile(thr, fast_math=FM)

param = np.complex64(jcom * g * dt * np.ones(size_box))
gparam = thr.to_device(param)

#####################################################################################################
#The following lines are where we write some KERNELS for posterior compilation.
#src expects one input and returns one output
#src2 expects two inputs and returns one output

# The General Structure of these kernels is the Following:
# -  <% %> code inside these symbols is Python code and in our case
# it simply generates a C-type that corresponds to the type of the input we give it
# - An actual KERNEL, whose arguments are the inputs and the output (in Global
#memory) and
# - whose purpose is to execute the code inside {} (pretty much like a C function).
#In both cases we declare the constant i which contains the index of a given array.
#We then say that the output array is the result of acting with a given function module
#on the input(s). Notice that to call the function module we use ${func}. In
# essence ${expression} executes a Python expression.

in_dtype = np.complex64

src="""
	<%
	in_ctype = dtypes.ctype(in_dtype)
	out_ctype = dtypes.ctype(out_dtype)
	%>
	KERNEL void Func(
		GLOBAL_MEM ${out_ctype} *edest,
		GLOBAL_MEM ${in_ctype} *a)
	{
		const SIZE_T i = get_local_id(0);
		edest[i]=${func}(a[i]);
	}
	"""

src2="""
	<%
        in_ctype1 = dtypes.ctype(in_dtype1)
	in_ctype2 = dtypes.ctype(in_dtype2)
        out_ctype = dtypes.ctype(out_dtype)
	%>
	KERNEL void Func2(
		GLOBAL_MEM ${in_ctype1} *a,
		GLOBAL_MEM ${in_ctype2} *b,
		GLOBAL_MEM ${out_ctype} *dest)
	{
		const SIZE_T i = get_local_id(0);
		dest[i] = ${func}(a[i],b[i]);
	}
	"""

#We compile the KERNELS against a thread thus producing GPU programs. Notice that
# require a rendering dictionary (render_kwds) to tell the KERNEL what in/out_dtype actually is
# (in our case np.complex64). Note that we also pass the function module necessary.

program_exp = thr.compile(src,
			render_kwds=dict(in_dtype=in_dtype,
					out_dtype=in_dtype,
					func=functions.exp(in_dtype)),
			fast_math=FM)

<<<<<<< HEAD
=======
#program_pow = thr.compile(src,
#                        render_kwds=dict(in_dtype=in_dtype,
#                                         out_dtype=in_dtype,
#                        		 func=functions.pow(in_dtype)),
#                        fast_math=FM)

program_norm = thr.compile(src,
                        render_kwds=dict(in_dtype=in_dtype,
					 out_dtype=in_dtype,
					 func=functions.norm(in_dtype)),
			fast_math=FM)

>>>>>>> 7efc96edd4d67e648fe2e3feec7655371acc6c7c
program_mul = thr.compile(src2,
                        render_kwds=dict(in_dtype1=in_dtype,
					 in_dtype2=in_dtype,
					 out_dtype=in_dtype,
					 func=functions.mul(in_dtype,in_dtype)),
			fast_math=FM)

#Once we have the pprograms we need, it is a simply matter of using their member functions almost as
# if they were simple Python functions. It is more convinient to shortcut this process somewhat (
#which is exactly what the next two lines do.
multiply_them = program_mul.Func2
Exp = program_exp.Func
<<<<<<< HEAD
=======
Norm = program_norm.Func
#Pow = program_pow.Func
>>>>>>> 7efc96edd4d67e648fe2e3feec7655371acc6c7c

#####################################################################################################

erstream = open(folder+'/error.txt','w')
start_time = timeit.default_timer()

#The Lines mentioned in the below cycle are the steps described on the report on page 3.
for t in range(len(time)):

	#Line 1
	#Compute psi in Fourier space
	fftc(output=gpsim,input=gpsi)

	#Line 2
	#Evolve for a half step in Fourier space
	multiply_them(gpsim,glin,gpsim,local_size=size_box,global_size=size_box)
	#return to direct space
	fftc(output=gpsi,input=gpsim,inverse=True)

	#Line 3
	#Evolve for a  full step
	psi = gpsi.get()
	nonlin = param * np.conj(psi) * psi
	gnonlin = thr.to_device(nonlin)
	Exp(gnonlin,gnonlin,local_size=size_box,global_size=size_box)
	multiply_them(gpsi,gnonlin,gpsi,local_size=size_box,global_size=size_box)

	#Line 4
	#Go back to Fourier space
	fftc(output=gpsim,input=gpsi)

	#Line 5
	#Evolve for a half-step in Fourier space
	multiply_them(gpsim,glin,gpsim,local_size=size_box,global_size=size_box)
	#Finish with Inverse transform
	fftc(output=gpsi,input=gpsim,inverse=True)

	if (t % 1000) == 0:
		#Plot the results, compare with the analytical expression in order to find
		#the relative error.
		plt.clf()
		psi = gpsi.get()
		plt.plot(x,abs(psi)**2,'b')
		plt.hold(True)
                if g > 0.0: psi_analytic=np.complex64(bright_func(x,x0=initpos,t=time[t]))
                if g < 0.0: psi_analytic=np.complex64(dark_func(x,x0=initpos,theta=0.0,psi_0=1.0,t=time[t])*envelop(L,x))
		plt.plot(x,abs(psi_analytic)**2,'r')
		plt.ylim(vmin,vmax)
		fname='/iter%05d.png'%(t+1)
		plt.savefig(folder+fname)

		error = simps(abs(psi-psi_analytic)**2) / simps(abs(psi)**2)
		erstream.write(str(error)+'\n')

ttaken=timeit.default_timer() - start_time
print ttaken
erstream.close()

#Forcefully free critical resources. Similar to popping a context in CUDA.
thr.release()
